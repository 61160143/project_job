import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/jobposting',
    name: 'Jobposting',
    component: () => import('../views/Jobpostings/Jobpostings.vue')
  },
  {
    path: '/jobseeker',
    name: 'JobSeeker',
    component: () => import('../views/JobSeeker/JobSeeker.vue')
  },
  {
    path: '/company',
    name: 'Company',
    component: () => import('../views/Company.vue')
  },
  {
    path: '/position',
    name: 'Position',
    component: () => import('../views/Position.vue')
  },
  {
    path: '/managejobpostings',
    name: 'ManageJobpostings',
    component: () => import('../views/Jobpostings/ManageJobpostings.vue')
  },
  {
    path: '/registerjobseeker',
    name: 'RegisterJobseeker',
    component: () => import('../views/RegisterJobSeeker/Register.vue')
  },
  {
    path: '/registercompany',
    name: 'RegisterCompany',
    component: () => import('../views/RegisterCompany/Register.vue')
  },
  {
    path: '/detail',
    name: 'Detail',
    component: () => import('../views/JobDetail.vue')
  },
  {
    path: '/resume',
    name: 'Resume',
    component: () => import('../views/Resume/Resume.vue')
  },
  {
    path: '/find',
    name: 'FindJobs',
    component: () => import('../views/FindJobs/FindJobs.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Auth/Login.vue')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('../views/Auth/Register.vue')
  },
  {
    path: '/profile',
    name: 'profile',
    // lazy-loaded
    component: () => import('../views/Auth/Profile.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const publicPages = ['/', '/find', '/registerjobseeker', '/registercompany', '/detail', '/login', '/register', '/home']
  const authRequired = !publicPages.includes(to.path)
  const loggedIn = localStorage.getItem('user')
  // trying to access a restricted page + not logged in
  // redirect to login page
  if (authRequired && !loggedIn) {
    next('/login')
  } else {
    next()
  }
})
export default router
