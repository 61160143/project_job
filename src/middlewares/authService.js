import axios from 'axios'

const API_URL = 'http://localhost:3000/auth/'

class AuthService {
  login (user) {
    return axios
      .post(API_URL + 'signin', {
        username: user.username,
        password: user.password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem('user', JSON.stringify(response.data))
        }
        return response.data
      })
  }

  logout () {
    localStorage.removeItem('user')
  }

  register (user) {
    console.log(user)
    if (user.roles[0] === 'company') {
      return axios.post(API_URL + 'signup', {
        username: user.username,
        email: user.email,
        password: user.password,
        roles: user.roles,
        company: user.ref
      })
    } else {
      return axios.post(API_URL + 'signup', {
        username: user.username,
        email: user.email,
        password: user.password,
        roles: user.roles,
        jobseeker: user.ref
      })
    }
  }
}

export default new AuthService()
