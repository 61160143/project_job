const jobseekerService = {
  jobseekerList: [
    {
      id: 1,
      name: 'Felidas',
      email: '61160143@go.buu.ac.th',
      tel: '6666666666'
    },
    { id: 2, name: 'Talue', email: '61160001@go.buu.ac.th', tel: '5555555555' }
  ],
  lastId: 3,
  addJobSeeker (jobseeker) {
    jobseeker.id = this.lastId++
    this.jobseekerList.push(jobseeker)
  },
  updateJobSeeker (jobseeker) {
    const index = this.jobseekerList.findIndex(item => item.id === jobseeker.id)
    this.jobseekerList.splice(index, 1, jobseeker)
  },
  deleteJobSeeker (jobseeker) {
    const index = this.jobseekerList.findIndex(item => item.id === jobseeker.id)
    this.jobseekerList.splice(index, 1)
  },
  getJobSeekers () {
    return [...this.jobseekerList]
  }
}
export default jobseekerService
